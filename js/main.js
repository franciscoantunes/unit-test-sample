const abbrString = (string) => {
  return string.replace(/([a-z])\w+/ig, function(word) {
    return (word.length > 3) ? word.charAt(0) + word.substring(1, (word.length - 1)).length + word.charAt(word.length - 1) : word;
  });
};

module.exports = abbrString;
