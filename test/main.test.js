const assert = require('chai').assert;
const abbrString = require('../js/main');
const faker = require('../js/lib/faker');

describe('String abbreviation 1', () => {
  it('should return the abbreviation of the string based on docs', () => {
    assert.equal(abbrString('elephant-rides are really fun!'), 'e6t-r3s are r4y fun!')
  });
});


describe('String abbreviation 2', () => {
  it('should have a number within the string when string length is greater than 3', () => {
    const sentence = faker.lorem.sentences();
    const words = sentence.split(/\W+/ig);
    const results = abbrString(sentence);
    const resultsWords = results.split(/\W+/ig);

    words.map((word, i) => {
      if (word.length > 3) assert.equal(word.length - 2, parseInt(resultsWords[i].match(/\d+/g)));
    })
  });
});