# Unit testing sample

To run this, I'd suggest to install mocha and static serve globally using `npm i -g mocha static-server`. Then `npm i`

To run the server:
`static-server`

To test:
`mocha`
